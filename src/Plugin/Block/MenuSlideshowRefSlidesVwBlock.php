<?php

namespace Drupal\drowl_header_slides\Plugin\Block;

/**
 * Provides a 'DROWL Header Slides Menu Slideshow Slide Block (Viewport Width)' Block.
 *
 * @Block(
 *   id = "drowl_header_menu_slideshow_ref_vw_block",
 *   admin_label = @Translation("DROWL Header Slides Menu Slideshow Slide Block (Viewport Width)"),
 *   category = @Translation("DROWL Header Slides"),
 * )
 */
class MenuSlideshowRefSlidesVwBlock extends MenuSlideshowRefSlidesBlock
{
  /**
   * The view mode to render the media header slideshow in.
   *
   * @var string
   */
  protected $view_mode = 'viewport_width';
}
