<?php

namespace Drupal\drowl_header_slides\Plugin\Block;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\menu_link_content\MenuLinkContentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Drupal\media\Entity\Media;
use Drupal\Component\Uuid\Uuid;


/**
 * Provides a 'DROWL Header Slides Menu Slideshow Slide Block (Page Width)' Block.
 *
 * @Block(
 *   id = "drowl_header_menu_slideshow_ref_block",
 *   admin_label = @Translation("DROWL Header Slides Menu Slideshow Slide Block (Page Width)"),
 *   category = @Translation("DROWL Header Slides"),
 * )
 */
class MenuSlideshowRefSlidesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  const MENU_LINK_CONTENT_ENTITY_TYPE = 'menu_link_content';

  /**
   * The entity type manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type manager object.
   *
   * @var \Drupal\Core\Menu\MenuLinkManager
   */
  protected $menuLinkManager;

  /**
   * The entity repository object.
   *
   * @var \Drupal\Core\Entity\EntityRepository
   */
  protected $entityRepository;

  /**
   * The MenuActiveTrail object.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrail
   */
  protected $menuActiveTrail;

  /**
   * The ConfigFactory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The view mode to render the media header slideshow in.
   *
   * @var string
   */
  protected $view_mode = 'full';

  /**
   * Class constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, MenuLinkManagerInterface $menuLinkManager, EntityRepositoryInterface $entityRepository, MenuActiveTrailInterface $menuActiveTrail, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->menuLinkManager = $menuLinkManager;
    $this->entityRepository = $entityRepository;
    $this->menuActiveTrail = $menuActiveTrail;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.menu.link'),
      $container->get('entity.repository'),
      $container->get('menu.active_trail'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $mediaSlideshowEntity = $this->determineActiveTrailMediaHeaderSlideEntity();
    if (!empty($mediaSlideshowEntity)) {
      /**
       * @var $mediaSlideshowEntity \Drupal\media\Entity\Media
       */
      if (!empty($mediaSlideshowEntity) && $mediaSlideshowEntity->access('view')) {
        // Return the rendered media entity:
        $build = \Drupal::entityTypeManager()->getViewBuilder('media')->view($mediaSlideshowEntity, $this->view_mode);
        return $build;
      }
    }
    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    //Every new route this block will rebuild
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }

  /**
   * Returns the currently active MenuLinkContent entity.
   *
   * @return \Drupal\media\Entity\Media|null
   */
  protected function determineActiveTrailMediaHeaderSlideEntity(): ?Media {
    // Cache per site call:
    $menuActiveLinkEntity = &drupal_static(__FUNCTION__, null);
    if (isset($menuActiveLinkEntity)) {
      return $menuActiveLinkEntity ?: null;
    }

    $candidateMenuLinkIds = $this->determineActiveTrailsMenuLinkIds();
    $i = 0;
    foreach ($candidateMenuLinkIds as $key => $candidateMenuLinkId) {
      $menuLinkContentEntity = $this->getMenuLinkContentEntityFromUuidString($candidateMenuLinkId);
      if (!empty($menuLinkContentEntity) && $menuLinkContentEntity->hasField('field_slideshow_ref')) {
        /**
         * @var $mediaSlideshowEntity \Drupal\media\Entity\Media
         */
        $mediaSlideshowEntity = $menuLinkContentEntity->field_slideshow_ref->entity;
        if (!empty($mediaSlideshowEntity)) {
          // ONLY return the entity if it's the currently displayed
          // menu item or has field_slideshow_inherit enabled!
          // Other menu items are wrong!
          if ($i == 0) {
            // This is the menu item of the node itself
            return $mediaSlideshowEntity;
          } elseif ($menuLinkContentEntity->hasField('field_slideshow_inherit')) {
            // The parent menu item has a field_slideshow_inherit field.
            /**
             * @var $inheritFromParent boolean
             */
            $inheritFromParent = !empty($menuLinkContentEntity->get('field_slideshow_inherit')->value);
            if ($inheritFromParent) {
              // Inherit from parent is enabled. Return the parent as slideshow provider:
              return $mediaSlideshowEntity;
            }
          }
        }
      }
      $i++;
    }

    return null;
  }

  /**
   * Returns an array of all active trail menu link UUIDs, starting
   * with the current node, up to front as last item, as they may contain
   * a header slideshow selection with field_slideshow_inherit enabled.
   *
   * This allows to travel up the list, until such an entry is found or
   * we can be sure there's no such entry.
   *
   * @return array
   */
  protected function determineActiveTrailsMenuLinkIds(): array {
    $searchInMenus = \Drupal::config('drowl_header_slides.settings')->get('menus');
    $activeTrailIds = [];
    if (!empty($searchInMenus)) {
      foreach ($searchInMenus as $menu_id) {
        $activeTrailIds = $this->menuActiveTrail->getActiveTrailIds($menu_id);
        // Return the first found active trail.
        // @todo This should be improved, perhaps better merge candidates
        // from all selected menus? (But in 90% this is enough).
        if (!empty($activeTrailIds) && (count($activeTrailIds) > 1 || !empty(reset($activeTrailIds)))) {
          // IMPORTANT: $activeTrailIds[''] is also returned for empty results
          // with an empty array, so wo have to sort that out!

          // Check if the currently viewed entity has a menu item itself,
          // otherwise it will not appear in the trail, but we have to indicate
          // this with an empty first entry in the $activeTrailIds
          // to show we're returning parent entities.
          $menuActiveLink = $this->menuActiveTrail->getActiveLink($menu_id);
          if (empty($menuActiveLink)) {
            // Prepend this placeholder value to the results to count all
            // other results as parent items and upper methods can see that
            // there's no direct / own menu item returned:
            array_unshift($activeTrailIds, 'ONLY-PARENT-ITEMS-FOUND-PLACEHOLDER');
          }
          break;
        }
      }
    }

    return $activeTrailIds;
  }

  /**
   * Returns the menu_link_content entity from the given $uuidString.
   *
   * @param string $uuidString
   *   The UUID string, for example menu_link_content:6887e19d-4426-47e3-afa9
   * @return \Drupal\menu_link_content\MenuLinkContentInterface|null
   */
  private function getMenuLinkContentEntityFromUuidString(string $uuidString): ?MenuLinkContentInterface {
    $parts = explode(':', $uuidString);
    // Skip if one part is empty:
    if (empty($parts[0]) || empty($parts[1])) {
      return null;
    }
    $pluginId = $parts[0];
    $uuid = $parts[1];

    // Skip if the item has no valid UUID:
    if (!Uuid::isValid($uuid)) {
      return null;
    }

    if (is_a($pluginId[0], MenuLinkContentInterface::class)) {
      throw new \Exception('Entity of type menu_link_content expected, but was "' . $parts[0] . '"');
    }

    $menuActiveLinkEntity = $this->entityRepository->loadEntityByUuid(self::MENU_LINK_CONTENT_ENTITY_TYPE, $parts[1]);
    if (!empty($menuActiveLinkEntity)) {
      return $menuActiveLinkEntity;
    }
    return null;
  }
}
