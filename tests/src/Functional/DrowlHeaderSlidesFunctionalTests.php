<?php

namespace Drupal\Tests\drowl_header_slides\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group drowl_header_slides
 */
class DrowlHeaderSlidesFunctionalTests extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    "block_content",
    "inline_entity_form",
    "drowl_media",
    "drowl_media_types",
    "menu_item_extras",
    'drowl_header_slides',
    'node',
    'test_page_test',
    "blazy",
    "crop",
    "content_translation",
    "field",
    "file",
    "image",
    "language",
    "layout_builder",
    "link",
    "media",
    "media_library",
    "options",
    "path",
    "responsive_image",
    "taxonomy",
    "text",
    "views",
    "entity_access_by_role_field",
    "fences",
    "field_group",
    "file_download_link",
    "focal_point",
    "link_attributes",
    "media_library_edit",
    "micon",
    "slick",
    "smart_trim",
    "svg_image",
    "svg_image_responsive",
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-drowl-header-slides');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm deinstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
  }

  /**
   * Tests "access drowl_header_slides settings" permission.
   */
  public function testAccess() {
    $session = $this->assertSession();
    // Check access as admin:
    $this->drupalGet('/admin/config/system/drowl-header-slides');
    $session->statusCodeEquals(200);
    $this->drupalLogout();
    // Check access as user with correct permissions:
    $this->drupalLogin($this->drupalCreateUser(['access drowl_header_slides settings']));
    $this->drupalGet('/admin/config/system/drowl-header-slides');
    $session->statusCodeEquals(200);
    $this->drupalLogout();
    // Check access as user without permissions:
    $this->drupalLogin($this->user);
    $this->drupalGet('/admin/config/system/drowl-header-slides');
    $session->statusCodeEquals(403);
    $this->drupalLogout();
    // Check access as anonymous:
    $this->drupalGet('/admin/config/system/drowl-header-slides');
    $session->statusCodeEquals(403);
  }

  /**
   * Tests if the config exists and we can add edit and delete.
   */
  public function testDrowlSliderConfigExists() {
    $session = $this->assertSession();
    $this->drupalGet('/admin/config/system/drowl-header-slides');
    $session->statusCodeEquals(200);
    // @todo Does this option exist?
    $this->drupalGet('/admin/config/system/drowl-header-slides/delete');
    $session->statusCodeEquals(200);
    // @todo correct?
    $this->drupalGet('/admin/config/system/drowl-header-slides/edit');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests the creation of a Fallback Block (Page Width)
   */
  public function testPlaceSlideshowBlockFallbackPageWidth() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/structure/block/add/views_block%3Adrowl_header_slideshow_fallback-block_page_width/stark');
    $session->statusCodeEquals(200);
    $page->checkField('edit-settings-label-display');
    $page->checkField('edit-settings-label-display');
    $page->fillField('edit-visibility-request-path-negate-1', '1');
    $page->fillField('edit-id', 'test_block');
    $page->fillField('edit-region', 'header');
    $page->pressButton('edit-actions-submit');
    // @todo Check if the Block exists on the front page:
    // $this->drupalGet('<front>');
    // $session->elementExists('css', 'test_block');
  }

  /**
   * Tests the creation of a Fallback Block (Viewport Width)
   */
  public function testPlaceSlideshowBlockFallbackViewportWidth() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/structure/block/add/views_block%3Adrowl_header_slideshow_fallback-block_viewport_width/stark');
    $session->statusCodeEquals(200);
    $page->checkField('edit-settings-label-display');
    $page->checkField('edit-settings-label-display');
    $page->fillField('edit-visibility-request-path-negate-1', '1');
    $page->fillField('edit-id', 'test_block');
    $page->fillField('edit-region', 'header');
    $page->pressButton('edit-actions-submit');
    // @todo Check if the Block exists on the front page:
    // $this->drupalGet('<front>');
    // $session->elementExists('css', 'test_block');
  }

  /**
   * Tests the creation of a Header Slides Menu Slideshow Slide Block.
   *
   * @todo This will lead to an error, see https://www.drupal.org/project/drowl_header_slides/issues/3271413
   */
  public function testPlaceSlideshowBlockHeaderSlidesMenuSlideshowSlideBlock() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/structure/block/add/drowl_header_menu_slideshow_ref_block/stark');
    $session->statusCodeEquals(200);
    $page->checkField('edit-settings-label-display');
    $page->checkField('edit-settings-label-display');
    $page->fillField('edit-visibility-request-path-negate-1', '1');
    $page->fillField('edit-id', 'test_block');
    $page->fillField('edit-region', 'header');
    $page->pressButton('edit-actions-submit');
    // @todo Check if the Block exists on the front page:
    // $this->drupalGet('<front>');
    // $session->elementExists('css', 'test_block');
  }

  /**
   * Tests the creation of a page width block list.
   *
   * Tests the creation of a
   * DROWL Header Slides > Referenced Slides: Page Width > Block.
   */
  public function testPlaceSlideshowBlockPageWidthList() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/structure/block/add/views_block%3Adrowl_headerslides_slideshow_ref-block_page_width/stark');
    $session->statusCodeEquals(200);
    $page->checkField('edit-settings-label-display');
    $page->checkField('edit-settings-label-display');
    $page->fillField('edit-visibility-request-path-negate-1', '1');
    $page->selectFieldOption('edit-settings-context-mapping-nid', '@node.node_route_context:node');
    $page->fillField('edit-id', 'test_block');
    $page->fillField('edit-region', 'header');
    $page->pressButton('edit-actions-submit');
    // @todo Check if the Block exists on the front page:
    // $this->drupalGet('<front>');
    // $session->elementExists('css', 'test_block');
  }

  /**
   * Tests the creation of a viewport width block list.
   *
   * Tests the creation of a
   * DROWL Header Slides > Referenced Slides: Viewport Width > Block.
   */
  public function testPlaceSlideshowBlockViewportWidthList() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/structure/block/add/views_block%3Adrowl_headerslides_slideshow_ref-block_viewport_width/stark');
    $session->statusCodeEquals(200);
    $page->checkField('edit-settings-label-display');
    $page->checkField('edit-settings-label-display');
    $page->fillField('edit-visibility-request-path-negate-1', '1');
    $page->selectFieldOption('edit-settings-context-mapping-nid', '@node.node_route_context:node');
    $page->fillField('edit-id', 'test_block');
    $page->fillField('edit-region', 'header');
    $page->pressButton('edit-actions-submit');
    // @todo Check if the Block exists on the front page:
    // $this->drupalGet('<front>');
    // $session->elementExists('css', 'test_block');
  }

  /**
   * Tests if the config is working correctly.
   */
  public function testConfig() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/config/system/drowl-header-slides');
    $session->statusCodeEquals(200);
    // @todo Continue, here once this issue is fixed: https://www.drupal.org/project/drowl_header_slides/issues/3271416
  }

}
